# PyCharm Language Chinese 中文化

## 下載

[官方網站](https://www.jetbrains.com/pycharm/)

**新版** - 2019.1.3

> 說個小抱怨：
> 這版單獨執行PY檔案要設定 interpreter 
> 每次打開都要設置一次
> 除非你建成專案才會幫你存

[官方載點][1]

[檔案較驗-sha256][3]：505af5a257d87402f2dc566cac618b3fcbe131c0499072c7963b56549c77729b

[1]:https://download-cf.jetbrains.com/python/pycharm-community-2019.1.3.exe
[3]:https://download.jetbrains.com/python/pycharm-community-2019.1.3.exe.sha256

**舊版** - 2018.2.2

[官方載點][4]

[檔案較驗-sha256][6]：5a462e04ac1cfbb3eff1ed5887d0a66a72139138973f1cbda83e373a9bd03ddf

[4]:https://download-cf.jetbrains.com/python/pycharm-community-2019.2.2.exe
[6]:https://download.jetbrains.com/python/pycharm-community-2018.2.2.exe.sha256

## 中文化說明 *新舊版皆相同

下載所需語言檔案至 PyCharm 安裝目錄下的 lib 目錄

是 lib 目錄  不是 bin 目錄

是 lib 目錄  不是 bin 目錄

是 lib 目錄  不是 bin 目錄

完成後重啟程式~~~

**新版** (版本2019.1.1適用) *繁簡體 合一

[中文化][9]

**舊版** (版本2018.2.2適用)

[繁體][7]

[簡體][8]

[7]:https://github.com/tommy924/PyCharm_Language_Chinese/blob/master/file/translate/resources_tw.jar?raw=true
[8]:https://github.com/tommy924/PyCharm_Language_Chinese/blob/master/file/translate/resources_cn.jar?raw=true
[9]:https://github.com/tommy924/PyCharm_Language_Chinese/blob/master/file/translate/resources_zh.jar?raw=true

*主要是掃描WebStorm的翻譯，做的這個中文語言包，漢化程度有限...

## 預覽(新版)

### 繁體

![image](images/5.png)

*主要是掃描WebStorm的翻譯，做的這個中文語言包，漢化程度有限...

## 預覽(舊版) *新版懶得截圖了Zzz..

### 繁體

![image](images/1.png)

![image](images/2.png)

### 簡體

![image](images/3.jpg)

![image](images/4.jpg)

## 原文

[PyCharm Chinese Language Pack（中文语言包）](https://github.com/ewen0930/PyCharm-Chinese)
[pycharm安装及设置中文](https://blog.csdn.net/LINZHENYU1996/article/details/79301469)
